FROM python:3.7-alpine

RUN pip install requests
RUN pip install influxdb

COPY . /app
WORKDIR /app